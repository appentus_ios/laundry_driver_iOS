//
//  File.swift
//  Laundry Driver
//
//  Created by Appentus Technologies on 12/25/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import Foundation
class User_Info{
    
    static let shared = User_Info()
    var customer_id = [String]()
    var customer_name = [String]()
    var customer_email = [String]()
    var customer_mobile = [String]()
    var customer_password = [String]()
    var customer_password_string = [String]()
    var customer_country_code = [String]()
    var customer_profile = [String]()
    var customer_city = [String]()
    var customer_device_type = [String]()
    var customer_device_token = [String]()
    var insert_date = [String]()
    var customer_remaining_amount = [String]()
    var customer_area_id = [String]()


 func remove_all() {
    self.customer_id = [String]()
    self.customer_name = [String]()
    self.customer_email = [String]()
    self.customer_mobile = [String]()
    self.customer_password = [String]()
    self.customer_password_string = [String]()
    self.customer_country_code = [String]()
    self.customer_profile = [String]()
    self.customer_city = [String]()
    self.customer_device_type = [String]()
    self.customer_device_token = [String]()
    self.insert_date = [String]()
    self.customer_remaining_amount = [String]()
    self.customer_area_id = [String]()
    

    }
    
    func set_data(dict:[String : Any]) {
        self.remove_all()
        let result_arr = dict["result"] as! [[String:Any]]
        for dict_result in result_arr {
            //self.customer_order_id.append("\(dict_result["customer_order_id"]!)")
           
            self.customer_id.append("\(dict_result["customer_id"]!)")
            self.customer_name.append("\(dict_result["customer_name"]!)")
            self.customer_email.append("\(dict_result["customer_email"]!)")
            self.customer_mobile.append("\(dict_result["customer_mobile"]!)")
            self.customer_password.append("\(dict_result["customer_password"]!)")
            self.customer_password_string.append("\(dict_result["customer_password_string"]!)")
            self.customer_country_code.append("\(dict_result["customer_country_code"]!)")
            self.customer_profile.append("\(dict_result["customer_profile"]!)")
            self.customer_city.append("\(dict_result["customer_city"]!)")
            self.customer_device_type.append("\(dict_result["customer_device_type"]!)")
            self.customer_device_token.append("\(dict_result["customer_device_token"]!)")
            self.insert_date.append("\(dict_result["insert_date"]!)")
            self.customer_remaining_amount.append("\(dict_result["customer_remaining_amount"]!)")
            self.customer_area_id.append("\(dict_result["customer_area_id"]!)")
            

        }
     }
  }
