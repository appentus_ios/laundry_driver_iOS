//
//  Model.swift
//  Laundry Driver
//
//  Created by Appentus Technologies on 12/20/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import Foundation
class login_model {
    
    static let shared = login_model()
    
    var runner_id = String()
    var runner_name = String()
    var runner_mobile = String()
    var runner_username = String()
    var runner_password = String()
    var string = String()
    var runner_adate = String()
    var runner_status = String()
    var runner_device_type = String()
    var runner_device_token = String()
    
    
    
    func remove_all() {
        runner_id = String()
        runner_name = String()
        runner_mobile = String()
        runner_username = String()
        runner_password = String()
        string = String()
        runner_adate = String()
        runner_status = String()
        runner_device_type = String()
        runner_device_token = String()
        
    }
    
    func set_data(_ dict : NSDictionary) {
        print("dict",dict)
        self.remove_all()
        self.runner_id = "\(dict["runner_id"]!)"
        self.runner_name = "\(dict["runner_name"]!)"
        self.runner_mobile = "\(dict["runner_mobile"]!)"
        self.runner_username = "\(dict["runner_username"]!)"
        self.runner_password = "\(dict["runner_password"]!)"
        self.string = "\(dict["string"]!)"
        self.runner_adate = "\(dict["runner_adate"]!)"
        self.runner_status = "\(dict["runner_status"]!)"
        self.runner_device_type = "\(dict["runner_device_type"]!)"
        self.runner_device_token = "\(dict["runner_device_token"]!)"
        
    }
    
    
}
