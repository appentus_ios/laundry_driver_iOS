//
//  Action_Model.swift
//  Laundry Driver
//
//  Created by Appentus Technologies on 12/21/19.
//  Copyright © 2019 appentus. All rights reserved.
//


import Foundation

class Action_Model {
    
    static let shared = Action_Model()
    
    
    var customer_order_id = [String]()
    var order_id = [String]()
    var customer_id = [String]()
    var order_total_amount = [String]()
    var payment_mode = [String]()
    var order_date = [String]()
    var order_time = [String]()
    var order_status = [String]()
    var order_json = [String]()
    
    var customer_name = [String]()
    var pickup_date = [String]()
    var pickup_time = [String]()
    var delivery_date = [String]()
    var delivery_time = [String]()
    var customer_mobile = [String]()
    var customer_profile = [String]()
    var runner_name = [String]()
    
    
    var promo_discount = [String]()
    var order_vat = [String]()
    var order_delivery_charge = [String]()
    var order_collect_cash = [String]()
    var old_amount = [String]()
    var order_type = [String]()
    var assign_id = [String]()
    var runner_id = [String]()
    var assign_type = [String]()
    var assign_status = [String]()
    var assign_date = [String]()
    var runner_mobile = [String]()
    var runner_username = [String]()
    var runner_password = [String]()
    var string = [String]()
    var runner_adate = [String]()
    var runner_status = [String]()
    var runner_device_type = [String]()
    var runner_device_token = [String]()
    var schedule_pickup_id = [String]()
    var pickup_location = [String]()
    var pickup_latitude = [String]()
    var pickup_longitude = [String]()
    var address_line1 = [String]()
    var address_line2 = [String]()
    var address_title = [String]()
    var pickup_status = [String]()
    var instruction = [String]()
    var customer_email = [String]()
    var customer_password = [String]()
    var customer_password_string = [String]()
    var customer_country_code = [String]()
    var customer_city = [String]()
    var customer_device_type = [String]()
    var customer_device_token = [String]()
    var insert_date = [String]()
    var customer_remaining_amount = [String]()
    var customer_area_id = [String]()
    var main_order_id = [String]()
    
    
    func remove_all() {
        self.customer_order_id = [String]()
        self.order_id = [String]()
        self.customer_id = [String]()
        self.order_total_amount = [String]()
        self.payment_mode = [String]()
        self.order_date = [String]()
        self.order_time = [String]()
        self.order_status = [String]()
        
        self.order_id = [String]()
        self.customer_name = [String]()
        self.pickup_date = [String]()
        self.pickup_time = [String]()
        self.delivery_date = [String]()
        self.delivery_time = [String]()
        self.customer_mobile = [String]()
        self.customer_profile = [String]()
        self.runner_name = [String]()
        
        self.promo_discount = [String]()
        self.order_vat = [String]()
        self.order_delivery_charge = [String]()
        self.order_collect_cash = [String]()
        self.old_amount = [String]()
        self.order_type = [String]()
        self.assign_id = [String]()
        self.runner_id = [String]()
        self.assign_type = [String]()
        self.assign_status = [String]()
        self.assign_date = [String]()
        self.runner_mobile = [String]()
        self.runner_username = [String]()
        self.runner_password = [String]()
        self.string = [String]()
        self.runner_adate = [String]()
        self.runner_status = [String]()
        self.runner_device_type = [String]()
        self.runner_device_token = [String]()
        self.schedule_pickup_id = [String]()
        self.pickup_location = [String]()
        self.pickup_latitude = [String]()
        self.pickup_longitude = [String]()
        self.address_line1 = [String]()
        self.address_line2 = [String]()
        self.address_title = [String]()
        self.pickup_status = [String]()
        self.instruction = [String]()
        self.customer_email = [String]()
        self.customer_password = [String]()
        self.customer_password_string = [String]()
        self.customer_country_code = [String]()
        self.customer_city = [String]()
        self.customer_device_type = [String]()
        self.customer_device_token = [String]()
        self.insert_date = [String]()
        self.customer_remaining_amount = [String]()
        self.customer_area_id = [String]()
        self.main_order_id = [String]()
        
        self.order_json = [String]()
        
    
    }
    
    func set_data(dict:[String : Any],_ tag:Int) {
        self.remove_all()
        
        let result_arr = dict["result"] as! [[String:Any]]
        
        for dict_result in result_arr {
            var order_status_check = "\(dict_result["order_status"]!)"
            if tag == 0{
                if order_status_check == "0" || order_status_check == "3"{
                    //self.customer_order_id.append("\(dict_result["customer_order_id"]!)")
                    self.customer_order_id.append("\(dict_result["customer_order_id"]!)")
                    self.order_id.append("\(dict_result["order_id"]!)")
                    self.customer_id.append("\(dict_result["customer_id"]!)")
                    self.order_total_amount.append("\(dict_result["order_total_amount"]!)")
                    self.payment_mode.append("\(dict_result["payment_mode"]!)")
                    self.order_date.append("\(dict_result["order_date"]!)")
                    self.order_time.append("\(dict_result["order_time"]!)")
                    self.order_status.append("\(dict_result["order_status"]!)")
                    self.order_json.append("\(dict_result["order_json"]!)")
                    self.order_id.append("\(dict_result["order_id"]!)")
                    self.customer_name.append("\(dict_result["customer_name"]!)")
                    self.pickup_date.append("\(dict_result["pickup_date"]!)")
                    self.pickup_time.append("\(dict_result["pickup_time"]!)")
                    self.delivery_date.append("\(dict_result["delivery_date"]!)")
                    self.delivery_time.append("\(dict_result["order_time"]!)")
                    self.customer_mobile.append("\(dict_result["customer_mobile"]!)")
                    self.customer_profile.append("\(dict_result["customer_profile"]!)")
                    self.runner_name.append("\(dict_result["runner_name"]!)")
                    
                    self.promo_discount.append("\(dict_result["promo_discount"]!)")
                    self.order_vat.append("\(dict_result["order_vat"]!)")
                    self.order_delivery_charge.append("\(dict_result["order_delivery_charge"]!)")
                    self.order_collect_cash.append("\(dict_result["order_collect_cash"]!)")
                    self.old_amount.append("\(dict_result["old_amount"]!)")
                    self.order_type.append("\(dict_result["order_type"]!)")
                    self.assign_id.append("\(dict_result["assign_id"]!)")
                    self.runner_id.append("\(dict_result["runner_id"]!)")
                    self.assign_type.append("\(dict_result["assign_type"]!)")
                    self.assign_status.append("\(dict_result["assign_status"]!)")
                    self.assign_date.append("\(dict_result["assign_date"]!)")
                    self.runner_mobile.append("\(dict_result["runner_mobile"]!)")
                    self.runner_username.append("\(dict_result["runner_username"]!)")
                    self.runner_password.append("\(dict_result["runner_password"]!)")
                    self.string.append("\(dict_result["string"]!)")
                    self.runner_adate.append("\(dict_result["runner_adate"]!)")
                    self.runner_status.append("\(dict_result["runner_status"]!)")
                    self.runner_device_type.append("\(dict_result["runner_device_type"]!)")
                    self.runner_device_token.append("\(dict_result["runner_device_token"]!)")
                    self.schedule_pickup_id.append("\(dict_result["schedule_pickup_id"]!)")
                    self.pickup_location.append("\(dict_result["pickup_location"]!)")
                    self.pickup_latitude.append("\(dict_result["pickup_latitude"]!)")
                    self.pickup_longitude.append("\(dict_result["pickup_longitude"]!)")
                    self.address_line1.append("\(dict_result["address_line1"]!)")
                    self.address_line2.append("\(dict_result["address_line2"]!)")
                    self.address_title.append("\(dict_result["address_title"]!)")
                    self.pickup_status.append("\(dict_result["pickup_status"]!)")
                    self.instruction.append("\(dict_result["instruction"]!)")
                    self.customer_email.append("\(dict_result["customer_email"]!)")
                    self.customer_password.append("\(dict_result["customer_password"]!)")
                    self.customer_password_string.append("\(dict_result["customer_password_string"]!)")
                    self.customer_country_code.append("\(dict_result["customer_country_code"]!)")
                    self.customer_city.append("\(dict_result["customer_city"]!)")
                    self.customer_device_type.append("\(dict_result["customer_device_type"]!)")
                    self.customer_device_token.append("\(dict_result["customer_device_token"]!)")
                    self.insert_date.append("\(dict_result["insert_date"]!)")
                    self.customer_remaining_amount.append("\(dict_result["customer_remaining_amount"]!)")
                    self.customer_area_id.append("\(dict_result["customer_area_id"]!)")
                    self.main_order_id.append("\(dict_result["main_order_id"]!)")
                    
                    //self.main_order_id.append("\(dict_result["main_order_id"]!)")
                  
                }
            }
            else{
                if order_status_check == "\(tag)" {
                    //self.customer_order_id.append("\(dict_result["customer_order_id"]!)")
                    self.customer_order_id.append("\(dict_result["customer_order_id"]!)")
                    self.order_id.append("\(dict_result["order_id"]!)")
                    self.customer_id.append("\(dict_result["customer_id"]!)")
                    self.order_total_amount.append("\(dict_result["order_total_amount"]!)")
                    self.payment_mode.append("\(dict_result["payment_mode"]!)")
                    self.order_date.append("\(dict_result["order_date"]!)")
                    self.order_time.append("\(dict_result["order_time"]!)")
                    self.order_status.append("\(dict_result["order_status"]!)")
                    self.order_json.append("\(dict_result["order_json"]!)")

                    self.order_id.append("\(dict_result["order_id"]!)")
                    self.customer_name.append("\(dict_result["customer_name"]!)")
                    self.pickup_date.append("\(dict_result["pickup_date"]!)")
                    self.pickup_time.append("\(dict_result["pickup_time"]!)")
                    self.delivery_date.append("\(dict_result["delivery_date"]!)")
                    self.delivery_time.append("\(dict_result["order_time"]!)")
                    self.customer_mobile.append("\(dict_result["customer_mobile"]!)")
                    self.customer_profile.append("\(dict_result["customer_profile"]!)")
                    self.runner_name.append("\(dict_result["runner_name"]!)")
                    
                    self.promo_discount.append("\(dict_result["promo_discount"]!)")
                    self.order_vat.append("\(dict_result["order_vat"]!)")
                    self.order_delivery_charge.append("\(dict_result["order_delivery_charge"]!)")
                    self.order_collect_cash.append("\(dict_result["order_collect_cash"]!)")
                    self.old_amount.append("\(dict_result["old_amount"]!)")
                    self.order_type.append("\(dict_result["order_type"]!)")
                    self.assign_id.append("\(dict_result["assign_id"]!)")
                    self.runner_id.append("\(dict_result["runner_id"]!)")
                    self.assign_type.append("\(dict_result["assign_type"]!)")
                    self.assign_status.append("\(dict_result["assign_status"]!)")
                    self.assign_date.append("\(dict_result["assign_date"]!)")
                    self.runner_mobile.append("\(dict_result["runner_mobile"]!)")
                    self.runner_username.append("\(dict_result["runner_username"]!)")
                    self.runner_password.append("\(dict_result["runner_password"]!)")
                    self.string.append("\(dict_result["string"]!)")
                    self.runner_adate.append("\(dict_result["runner_adate"]!)")
                    self.runner_status.append("\(dict_result["runner_status"]!)")
                    self.runner_device_type.append("\(dict_result["runner_device_type"]!)")
                    self.runner_device_token.append("\(dict_result["runner_device_token"]!)")
                    self.schedule_pickup_id.append("\(dict_result["schedule_pickup_id"]!)")
                    self.pickup_location.append("\(dict_result["pickup_location"]!)")
                    self.pickup_latitude.append("\(dict_result["pickup_latitude"]!)")
                    self.pickup_longitude.append("\(dict_result["pickup_longitude"]!)")
                    self.address_line1.append("\(dict_result["address_line1"]!)")
                    self.address_line2.append("\(dict_result["address_line2"]!)")
                    self.address_title.append("\(dict_result["address_title"]!)")
                    self.pickup_status.append("\(dict_result["pickup_status"]!)")
                    self.instruction.append("\(dict_result["instruction"]!)")
                    self.customer_email.append("\(dict_result["customer_email"]!)")
                    self.customer_password.append("\(dict_result["customer_password"]!)")
                    self.customer_password_string.append("\(dict_result["customer_password_string"]!)")
                    self.customer_country_code.append("\(dict_result["customer_country_code"]!)")
                    self.customer_city.append("\(dict_result["customer_city"]!)")
                    self.customer_device_type.append("\(dict_result["customer_device_type"]!)")
                    self.customer_device_token.append("\(dict_result["customer_device_token"]!)")
                    self.insert_date.append("\(dict_result["insert_date"]!)")
                    self.customer_remaining_amount.append("\(dict_result["customer_remaining_amount"]!)")
                    self.customer_area_id.append("\(dict_result["customer_area_id"]!)")
                    self.main_order_id.append("\(dict_result["main_order_id"]!)")
                    
  
                    
                }
                
            }
            
           
        }
        
    }
    
    
    
}

