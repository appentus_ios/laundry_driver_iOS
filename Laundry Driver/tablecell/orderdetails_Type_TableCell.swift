//
//  orderdetails_Type_TableCell.swift
//  Laundry Driver
//
//  Created by appentus on 9/17/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit

class orderdetails_Type_TableCell: UITableViewCell {

    @IBOutlet weak var lbl_service_name: UILabel!
    @IBOutlet weak var lbl_category: UILabel!
    @IBOutlet weak var lbl_count: UILabel!
    @IBOutlet weak var lbl_unit_price: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
