//
//  lhub_2TableViewCell.swift
//  Laundry Driver
//
//  Created by appentus on 9/17/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit

class lhub_pickup_TableViewCell: UITableViewCell {

    @IBOutlet weak var collected_Lbl: UILabel!
    @IBOutlet weak var color_View: UIView!
    @IBOutlet weak var date_LBl: UILabel!
    @IBOutlet weak var inside_colorView: UIView!
    @IBOutlet weak var selection_Btn: UIButton!
    @IBOutlet weak var lbl_order_no: UILabel!
    @IBOutlet weak var lbl_pickuptime: UILabel!
    @IBOutlet weak var lbl_user_name: UILabel!
    @IBOutlet weak var lbl_runner_name: UILabel!
    @IBOutlet weak var lbl_user_address: UILabel!
    @IBOutlet weak var user_phone_no: UILabel!
    @IBOutlet weak var img_user: UIImageView!
    @IBOutlet weak var img_driver: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        date_LBl.layer.cornerRadius = 17.5
        date_LBl.clipsToBounds = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
