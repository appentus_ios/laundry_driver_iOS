//
//  lhub_1TableViewCell.swift
//  Laundry Driver
//
//  Created by appentus on 9/17/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit

class lhub_action_TableViewCell: UITableViewCell {

    @IBOutlet weak var lbl_order_id: UILabel!
    @IBOutlet weak var date_Lbl: UILabel!
    @IBOutlet weak var lbl_delivery_date: UILabelX!
    @IBOutlet weak var lbl_pickup_time: UILabel!
    @IBOutlet weak var lbl_delivery_time: UILabel!
    @IBOutlet weak var lbl_user_name: UILabel!
    @IBOutlet weak var lbl_user_ph_no: UILabel!
    @IBOutlet weak var lbl_user_address: UILabel!
    @IBOutlet weak var img_user_img: UIImageView!
    @IBOutlet weak var img_driver_img: UIImageView!
    @IBOutlet weak var lbl_driver_name: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        date_Lbl.layer.cornerRadius = 15
        date_Lbl.clipsToBounds = true
        lbl_delivery_date.layer.cornerRadius = 15
        lbl_delivery_date.clipsToBounds = true

        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
