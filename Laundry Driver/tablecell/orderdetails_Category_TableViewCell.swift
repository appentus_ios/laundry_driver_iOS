//
//  orderdetails_Category_TableViewCell.swift
//  Laundry Driver
//
//  Created by appentus on 9/17/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit

class orderdetails_Category_TableViewCell: UITableViewCell {

    @IBOutlet weak var lbl_unit: UILabel!
    @IBOutlet weak var lbl_item_name: UILabel!
    @IBOutlet weak var lbl_quantity: UILabel!
    @IBOutlet weak var lbl_total_amount: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
