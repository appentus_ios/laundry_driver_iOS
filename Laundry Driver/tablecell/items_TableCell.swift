//
//  items_TableCell.swift
//  Laundry Driver
//
//  Created by appentus on 9/17/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit

class items_TableCell: UITableViewCell {
    var count = 0
    
    @IBOutlet weak var lbl_item_name: UILabel!
    @IBOutlet weak var lbl_quantity: UITextField!
    @IBOutlet weak var lbl_price: UILabel!
    @IBOutlet weak var btn_positive: UIButton!
    @IBOutlet weak var btn_nehative: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBAction func btn_positive(_ sender: UIButton) {
        count += 1
        lbl_quantity.text = String(count)
        print(count)
    }
    @IBAction func btn_negative(_ sender: UIButton) {
        count -= 1
        lbl_quantity.text = String(count-1)
        print(count)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
