//
//  lhub_completed_TableViewCell.swift
//  Laundry Driver
//
//  Created by appentus on 9/17/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit

class lhub_completed_TableViewCell: UITableViewCell {

    @IBOutlet weak var selection_btn: UIButton!
    @IBOutlet weak var lbl_order_id: UILabel!
    @IBOutlet weak var lbl_user_name: UILabel!
    @IBOutlet weak var img_user_img: UIImageView!
    @IBOutlet weak var lbl_user_phoneno: UILabel!
    @IBOutlet weak var lbl_date: UILabel!
    
    @IBOutlet weak var lbl_user_Address: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
