//  lhub_VC.swift
//  Laundry Driver
//  Created by appentus on 9/17/19.
//  Copyright © 2019 appentus. All rights reserved.


import UIKit


class lhub_VC: UIViewController {
    @IBOutlet weak var segment_controller: UISegmentedControl!
    @IBOutlet weak var table_View: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Assign_Order(0)
//        Pickup_Order()
       // Delivery_Order()
        
        table_View.delegate = self
        table_View.dataSource = self
    }
    
    @IBAction func log_out_btn(_ sender: Any) {
        UserDefaults.standard.removeObject(forKey: "login_data")
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "login_VC") as! login_VC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btn_segment(_ sender: UISegmentedControl) {
        Assign_Order(sender.selectedSegmentIndex)
    }
    
    @IBAction func btn_goto_order_details(_ sender: UIButton) {
    
    }
    
    @objc func selected(_ sender: UIButton) {
        let nav = storyboard?.instantiateViewController(withIdentifier: "order_Details_VC") as! order_Details_VC
        self.present(nav, animated: true, completion: nil)
    }
    
    @objc func selected_2(_ sender: UIButton) {
        let nav = storyboard?.instantiateViewController(withIdentifier: "ready_to_delivered_VC") as! ready_to_delivered_VC
        self.present(nav, animated: true, completion: nil)
    
    }
    
    @objc func selected_3(_ sender: UIButton) {
        let nav = storyboard?.instantiateViewController(withIdentifier: "items_VC") as! items_VC
        nav.order_json = Action_Model.shared.order_json[sender.tag]
        self.present(nav, animated: true, completion: nil)
   
    }
    
}

extension lhub_VC: UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

            return Action_Model.shared.order_status.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch segment_controller.selectedSegmentIndex {
        case 0:
           let cell = tableView.dequeueReusableCell(withIdentifier: "lhub_action_TableViewCell", for: indexPath) as! lhub_action_TableViewCell
           
            cell.lbl_order_id.text = Action_Model.shared.main_order_id[indexPath.row]
            cell.lbl_user_name.text = Action_Model.shared.customer_name[indexPath.row]
            cell.lbl_delivery_time.text = Action_Model.shared.delivery_time[indexPath.row]
            cell.lbl_delivery_date.text = Action_Model.shared.delivery_date[indexPath.row]
            cell.lbl_user_ph_no.text = Action_Model.shared.customer_mobile[indexPath.row]
            cell.lbl_pickup_time.text = Action_Model.shared.pickup_time[indexPath.row]
            cell.date_Lbl.text = Action_Model.shared.pickup_date[indexPath.row]
            cell.lbl_driver_name.text = Action_Model.shared.runner_name[indexPath.row]
           cell.lbl_user_address.text = Action_Model.shared.address_line1[indexPath.row] + "," + Action_Model.shared.address_line2[indexPath.row] + "," +  Action_Model.shared.address_title[indexPath.row]
           return cell
       
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "lhub_pickup_TableViewCell", for: indexPath) as! lhub_pickup_TableViewCell
            
            cell.color_View.backgroundColor = UIColor.init(red: 254.0/255.0, green: 107.0/255.0, blue: 1.0/255.0, alpha: 1)
            cell.collected_Lbl.text = "To be Collected"
            cell.collected_Lbl.textColor = UIColor.init(red: 254.0/255.0, green: 107.0/255.0, blue: 1.0/255.0, alpha: 1)
            cell.date_LBl.backgroundColor = UIColor.init(red: 254.0/255.0, green: 107.0/255.0, blue: 1.0/255.0, alpha: 1)
            //cell.inside_colorView.backgroundColor = UIColor.init(red: 254.0/255.0, green: 107.0/255.0, blue: 1.0/255.0, alpha: 1)
           
            cell.lbl_user_name.text = Action_Model.shared.customer_name[indexPath.row]
            cell.lbl_order_no.text = Action_Model.shared.main_order_id[indexPath.row]
            cell.lbl_pickuptime.text = Action_Model.shared.pickup_time[indexPath.row]
            cell.date_LBl.text = Action_Model.shared.pickup_date[indexPath.row]
            cell.lbl_runner_name.text = Action_Model.shared.runner_name[indexPath.row]
            cell.lbl_user_address.text = Action_Model.shared.address_line1[indexPath.row] + "," + Action_Model.shared.address_line2[indexPath.row] + "," + Action_Model.shared.address_title[indexPath.row]
            cell.user_phone_no.text = Action_Model.shared.customer_mobile[indexPath.row] 
            
            cell.selection_Btn.removeTarget(self, action: #selector(selected_2(_:)), for: .touchUpInside)
            cell.selection_Btn.addTarget(self, action: #selector(selected_3(_:)), for: .touchUpInside)
            cell.selection_Btn.tag = indexPath.row
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "lhub_pickup_TableViewCell", for: indexPath) as! lhub_pickup_TableViewCell
            
            cell.color_View.backgroundColor = UIColor.init(red: 215.0/255.0, green: 27.0/255.0, blue: 96.0/255.0, alpha: 1)
            cell.collected_Lbl.text = "Ready to delivered"
            cell.collected_Lbl.textColor = UIColor.init(red: 215.0/255.0, green: 27.0/255.0, blue: 96.0/255.0, alpha: 1)
            cell.date_LBl.backgroundColor = UIColor.init(red: 215.0/255.0, green: 27.0/255.0, blue: 96.0/255.0, alpha: 1)
//             cell.inside_colorView.backgroundColor = UIColor.darkGray
            cell.lbl_user_name.text = Action_Model.shared.customer_name[indexPath.row]
            cell.lbl_order_no.text = Action_Model.shared.main_order_id[indexPath.row]
            cell.lbl_pickuptime.text = Action_Model.shared.pickup_time[indexPath.row]
            cell.date_LBl.text = Action_Model.shared.pickup_date[indexPath.row]
            cell.lbl_runner_name.text = Action_Model.shared.runner_name[indexPath.row]

            cell.selection_Btn.removeTarget(self, action: #selector(selected_3(_:)), for: .touchUpInside)
            cell.selection_Btn.addTarget(self, action: #selector(selected_2(_:)), for: .touchUpInside)
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "lhub_completed_TableViewCell", for: indexPath) as! lhub_completed_TableViewCell
              cell.selection_btn.addTarget(self, action: #selector(selected(_:)), for: .touchUpInside)
            cell.lbl_user_name.text = Action_Model.shared.customer_name[indexPath.row]
            cell.lbl_date.text =  Action_Model.shared.delivery_date[indexPath.row]
            cell.lbl_order_id.text = Action_Model.shared.main_order_id[indexPath.row]
            cell.lbl_user_Address.text = Action_Model.shared.address_line1[indexPath.row] + "," + Action_Model.shared.address_line2[indexPath.row] + "," + Action_Model.shared.address_title[indexPath.row]
            cell.lbl_user_phoneno.text = Action_Model.shared.order_total_amount[indexPath.row]
            print("******")
            print(Action_Model.shared.main_order_id[indexPath.row])
            print(Action_Model.shared.runner_mobile[indexPath.row])
            return cell
            
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "lhub_action_TableViewCell", for: indexPath) as! lhub_action_TableViewCell
    
            return cell

        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch segment_controller.selectedSegmentIndex {
        case 0:
            return 310
        case 1:
            return 280
        case 2:
            return 280
        case 3:
            return 260
            
        default:
            break
        }
        return 0
    }
    
}
extension lhub_VC{

    //    MARK: Assign order
    func Assign_Order(_ tag:Int) {
        func_ShowHud()
        let param = ["runner_id":login_model.shared.runner_id] as [String:Any]
        APIFunc.postAPI(url: k_base_url+"runner_assing_order", parameters: param) { (resp) in
            let status = return_status(resp)
            switch status{
            case .success:
                self.func_HideHud()
                self.func_ShowHud_Success(with: "\(resp["message"]!)")
                if tag == 0 {
                    Action_Model.shared.set_data(dict: resp, tag)
                } else if tag == 1 {
                    Action_Model.shared.set_data(dict: resp, 0)
                } else if tag == 2 {
                    Action_Model.shared.set_data(dict: resp, 3)
                } else if tag == 3 {
                    Action_Model.shared.set_data(dict: resp, 4)
                }
                
                //Pickup_Model.shared.set_data2(dict2: resp)
                self.table_View.reloadData()
                
            case .fail:
                self.func_HideHud()
                self.func_ShowHud_Error(with: "\(resp["message"]!)")
            case .error_from_api:
                self.func_HideHud()
                self.func_ShowHud_Error(with: resp["error_message"] as! String)
            }
        }
    }
    
//    func Pickup_Order() {
//        func_ShowHud()
//        let param = ["order_id":Action_Model.shared.order_id] as [String:Any]
//
//        APIFunc.postAPI(url: k_base_url+"order_pickup", parameters: param) { (resp) in
//            let status = return_status(resp)
//            switch status{
//            case .success:
//                self.func_HideHud()
//                self.func_ShowHud_Success(with: "\(resp["message"]!)")
//                Pickup_Model.shared.set_data2(dict2: resp)
//                self.table_View.reloadData()
//
//            case .fail:
//                self.func_HideHud()
//                self.func_ShowHud_Error(with: "\(resp["message"]!)")
//            case .error_from_api:
//                self.func_HideHud()
//                self.func_ShowHud_Error(with: resp["error_message"] as! String)
//            }
//        }
//    }

//    func Delivery_Order() {
//        func_ShowHud()
//        let param = ["order_id":Pickup_Model.shared.order_id] as [String:Any]
//        APIFunc.postAPI(url: k_base_url+"order_delivered", parameters: param) { (resp) in
//            let status = return_status(resp)
//            switch status{
//            case .success:
//                self.func_HideHud()
//                self.func_ShowHud_Success(with: "\(resp["message"]!)")
//
//                Delivery_Model.shared.set_data3(dict3: resp)
//                self.table_View.reloadData()
//
//            case .fail:
//                self.func_HideHud()
//                self.func_ShowHud_Error(with: "\(resp["message"]!)")
//            case .error_from_api:
//                self.func_HideHud()
//                self.func_ShowHud_Error(with: resp["error_message"] as! String)
//            }
//        }
//    }

}
