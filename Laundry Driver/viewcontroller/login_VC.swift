//
//  login_VC.swift
//  Laundry Driver
//
//  Created by appentus on 9/13/19.
//  Copyright © 2019 appentus. All rights reserved.


import UIKit

class login_VC: UIViewController {

    @IBOutlet weak var txt_username: UITextFieldX!
    @IBOutlet weak var txt_password: UITextFieldX!
    override func viewDidLoad() {
        super.viewDidLoad()
        login_model()
    }
    
    @IBAction func login_btn(_ sender: Any){
              if self.txt_username.text! == ""{
            self.func_ShowHud_Error(with: "Enter a valid user name.")
            
     }else
        if txt_password.text?.remove_white_string().count ?? 0 < 0 || txt_password.text?.remove_white_string().count ?? 0 < 6{
            self.func_ShowHud_Error(with: "password must be of 6 characters.")
            
        }else {
              login_by_mail(txt_username.text!, txt_password.text!)
        }
        
//        if txt_username.layer.borderColor == hexStringToUIColor(hex: "#89D52C").cgColor && txt_password.layer.borderColor == hexStringToUIColor(hex: "#89D52C").cgColor {
//            login_by_mail(txt_username.text!, txt_password.text!)
//
//        }
        
    }
    
//    {
//        let nav = storyboard?.instantiateViewController(withIdentifier: "lhub_VC") as! lhub_VC
//        self.present(nav, animated: true, completion: nil)
//    }
    
    
    
}

extension login_VC{
    //    MARK: login api
    func login_by_mail(_ email: String,_ password:String ) {
        func_ShowHud()
        self.txt_username.resignFirstResponder()
        self.txt_password.resignFirstResponder()
        let param = ["user_name":self.txt_username.text!,"user_password":self.txt_password.text!,"runner_device_token":fcm_token,"runner_device_type":"ios"] as [String:Any]
        
        APIFunc.postAPI(url: k_base_url+"runner_login", parameters: param) { (resp) in
            let status = return_status(resp)
            switch status{
            case .success:
                self.func_HideHud()
                
                let result_arr = resp["result"] as! NSArray
                let result = result_arr[0] as! NSDictionary
                UserDefaults.standard.set(result, forKey: "login_data")
                login_model.shared.set_data(result)
                
                let nav = self.storyboard?.instantiateViewController(withIdentifier: "lhub_VC") as! lhub_VC
                self.present(nav, animated: true, completion: nil)
            case .fail:
                self.func_HideHud()
                self.func_ShowHud_Error(with: "\(resp["message"]!)")
            case .error_from_api:
                self.func_HideHud()
                self.func_ShowHud_Error(with: resp["error_message"] as! String)
            }
        }
    }
}
