//  order_Details_VC.swift
//  Laundry Driver
//
//  Created by appentus on 9/17/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit

class order_Details_VC: UIViewController {

    @IBOutlet weak var tbl_View: UITableView!
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var lbl_phone_no: UILabel!
    @IBOutlet weak var lbl_delivery_time: UILabel!
    @IBOutlet weak var lbl_delivery_date: UILabel!
    @IBOutlet weak var lbl_address: UILabel!
    @IBOutlet weak var lbl_pickup_date: UILabel!
    @IBOutlet weak var lbl_pickup_time: UILabel!
    @IBOutlet weak var lbl_special_instruction: UILabel!
    @IBOutlet weak var lbl_grand_total: UILabel!
    
     var order_json = String()
     var dict_data = [[String:Any]]()

    var customer_name = [String]()
    var customer_email = [String]()
    var customer_mobile = [String]()
    var customer_password = [String]()
    var customer_password_string = [String]()
    var customer_country_code = [String]()
    var customer_profile = [String]()
    var customer_city = [String]()
    var customer_device_type = [String]()
    var customer_device_token = [String]()
    var result = [String]()
    var cloth_id = [String]()
    var cloth_qty = [String]()
    var cloth_name = [String]()
    var cloth_cost = [String]()
    var service_id = String()
    var cloth_rate = [String]()
    var service_name = [String]()
    
        override func viewDidLoad() {
        super.viewDidLoad()
        load_customer_info()
        order_Details_VC()
        Action_Model()
        load_user_details()
        tbl_View.delegate = self
        tbl_View.dataSource = self
    }
    
    @IBAction func back(_ sender: Any) {
    dismiss(animated: true, completion: nil)
    }
  }

 extension order_Details_VC: UITableViewDataSource, UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return User_Info.shared.customer_id.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "orderdetails_Type_TableCell", for: indexPath) as! orderdetails_Type_TableCell
            cell.lbl_category.text = self.service_name[indexPath.row]
            return cell
            
        } else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "orderdetails_Category_TableViewCell", for: indexPath) as! orderdetails_Category_TableViewCell
           // cell.lbl_price.text = self.cloth_cost[indexPath.row]
            cell.lbl_item_name.text = User_Info.shared.customer_name[indexPath.row]
//            cell.lbl_quantity.text = User_Info.shared.[indexPath.row]
//            cell.lbl_total_amount.text = User_Info.shared.cos[indexPath.row]
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    
        if indexPath.row == 0{
            return 100
        }
        else {
            return 100
        }
    }
}

 extension order_Details_VC {
    //    MARK: login api
    //func login_by_mail(_ email: String,_ password:String )
    func load_user_details(){
        func_ShowHud()
        let param = ["user_id":"1"]//login_model.shared.runner_id] as [String:Any]
        //let param = ["user_id":"1"] as [String:Any]
        APIFunc.postAPI(url: k_base_url+"get_user_info", parameters: param) { (resp) in
            let status = return_status(resp)
            switch status{
            case .success:
                self.func_HideHud()
//                let result_arr = resp["result"] as! [String:Any]
                User_Info.shared.set_data(dict:resp)
                DispatchQueue.main.async {
                    self.load_customer_info()
                }
            case .fail:
                self.func_HideHud()
                self.func_ShowHud_Error(with: "\(resp["message"]!)")
                
            case .error_from_api:
                self.func_HideHud()
                self.func_ShowHud_Error(with: resp["error_message"] as! String)
            }
        }
    }
    
//    func set_data() {
//        //        dict_data
//        lbl_one.text = "\(dict_data[0]["service_name"]!)"
//        steam_Lbl.text = "\(dict_data[1]["service_name"]!)"
//        wash_Bl.text = "\(dict_data[2]["service_name"]!)"
//        dry_Lbl.text = "\(dict_data[3]["service_name"]!)"
//
//    }
    
     func load_customer_info(){
         //lbl_one.text = "\(dict_data[0]["service_name"]!)"
        // result = "\(dict_data["result"]!)"
        if User_Info.shared.customer_name.count > 0 {
            lbl_name.text = User_Info.shared.customer_name[0]
            lbl_phone_no.text = "\(User_Info.shared.customer_country_code[0]) \(User_Info.shared.customer_mobile[0])"
            lbl_address.text = User_Info.shared.customer_city[0]
            
            let pickup_date = User_Info.shared.insert_date[0].components(separatedBy: " ")
            lbl_pickup_time.text = pickup_date[0]
            lbl_pickup_date.text = pickup_date[1]
            
            //        lbl_special_instruction.text = User_Info.shared.ins[0]
            //        lbl_delivery_time.text =  User_Info.shared.customer_name[0]
            //        lbl_delivery_date.text = User_Info.shared.customer_name[0]
        }


        customer_name = [String]()
        customer_email = [String]()
        customer_mobile = [String]()
        customer_password = [String]()
        customer_password_string = [String]()
        customer_country_code = [String]()
        customer_profile = [String]()
        customer_city = [String]()
        customer_device_type = [String]()
        customer_device_token = [String]()
        result = [String]()
        cloth_id = [String]()
        cloth_qty = [String]()
        cloth_name = [String]()
        cloth_cost = [String]()
        service_name = [String]()

        // self.tbl_View.reloadData()
    }
    
    func decode_order_json() {
       let data = order_json.data(using: .utf8)!
       do {
       if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [Dictionary<String,Any>]
       {
       print(jsonArray)
       dict_data = jsonArray
       } else {
       print("bad json")
       }
     } catch let error as NSError {
     print(error)
        }
    }
}

  extension String{
    func strigns_to_dict() -> [Dictionary<String,Any>]{
        let data = self.data(using: .utf8)!
        do {
            if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [Dictionary<String,Any>]
            {
                print(jsonArray)
                return jsonArray
            } else {
                print("bad json")
                return [["error":true]]
            }
        } catch let error as NSError {
            print(error)
            return [["error":true]]
        }
    }
}



