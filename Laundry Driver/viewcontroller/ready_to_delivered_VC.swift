//
//  ready_to_delivered_VC.swift
//  Laundry Driver
//
//  Created by appentus on 9/17/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit

class ready_to_delivered_VC: UIViewController {

    @IBOutlet weak var tbl_View: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tbl_View.delegate = self
        tbl_View.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func back(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
        
    }
 
}

extension ready_to_delivered_VC: UITableViewDataSource, UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "readytodelivered_type_tableViewCell", for: indexPath) as! readytodelivered_type_tableViewCell
            
            return cell
            
        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "readytodelivered_category_tableViewCell", for: indexPath) as! readytodelivered_category_tableViewCell
            
            return cell
            
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 60
        } else {
            return 40
        }
    }
}
