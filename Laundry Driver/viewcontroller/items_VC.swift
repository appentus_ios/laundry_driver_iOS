//  items_VC.swift
//  Laundry Driver
//  Created by appentus on 9/17/19.
//  Copyright © 2019 appentus. All rights reserved.

import UIKit

class items_VC: UIViewController {

    @IBOutlet weak var Wash_Fold_No_iron: UIView!
    @IBOutlet weak var dry_View: UIView!
    @IBOutlet weak var steam_View: UIView!
    @IBOutlet weak var wash_View: UIView!
    @IBOutlet weak var dry_Lbl: UILabel!
    @IBOutlet weak var steam_Lbl: UILabel!
    @IBOutlet weak var wash_Bl: UILabel!
    @IBOutlet weak var tbl_View: UITableView!
    @IBOutlet weak var lbl_one: UILabel!
    @IBOutlet weak var first_line_view: UIView!
    @IBOutlet weak var scnd_line_view: UIView!
    @IBOutlet weak var thrd_line_view: UIView!
    @IBOutlet weak var forth_line_view: UIView!
    
    
    var order_json = String()
    var dict_data = [[String:Any]]()
    var count = 0
    var cloth_id = [String]()
    var cloth_qty = [String]()
    var cloth_name = [String]()
    var cloth_cost = [String]()
    var service_id = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tbl_View.delegate = self
        tbl_View.dataSource  = self
        decode_order_json()
        set_data()
        load_data_by_tag(0)
    }
    
    @IBAction func back(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func btn_go_to_nxt(_ sender: Any) {
//        let nav = storyboard?.instantiateViewController(withIdentifier: "order_Details_VC") as! order_Details_VC
//     nav.order_json = Action_Model.shared.order
//        self.present(nav, animated: true, completion: nil)
    }
    @IBAction func btn_top_actions(_ sender: UIButton) {
        load_data_by_tag(sender.tag)
        if sender.tag == 0{
            first_line_view.backgroundColor = hexStringToUIColor(hex: "00CC32")
            lbl_one.textColor = hexStringToUIColor(hex: "00CC32")
        }
        else{
            first_line_view.backgroundColor = UIColor.clear
            lbl_one.textColor = UIColor.white
        }
         if sender.tag == 1{
            scnd_line_view.backgroundColor = hexStringToUIColor(hex: "00CC32")
            steam_Lbl.textColor = hexStringToUIColor(hex: "00CC32")
        }
         else{
             scnd_line_view.backgroundColor = UIColor.clear
             steam_Lbl.textColor = UIColor.white
        }
         if sender.tag == 2{
            thrd_line_view.backgroundColor = hexStringToUIColor(hex: "00CC32")
            wash_Bl.textColor =  hexStringToUIColor(hex: "00CC32")
        }
         else{
             thrd_line_view.backgroundColor = UIColor.clear
            wash_Bl.textColor = UIColor.white
        }
        if sender.tag == 3{
            forth_line_view.backgroundColor = hexStringToUIColor(hex: "00CC32")
            dry_Lbl.textColor = hexStringToUIColor(hex: "00CC32")
            
        }
        else{
            forth_line_view.backgroundColor = UIColor.clear
            dry_Lbl.textColor = UIColor.white
        }
    }
}

extension items_VC{
    func set_data() {
//        dict_data
        lbl_one.text = "\(dict_data[0]["service_name"]!)"
        steam_Lbl.text = "\(dict_data[1]["service_name"]!)"
        wash_Bl.text = "\(dict_data[2]["service_name"]!)"
        dry_Lbl.text = "\(dict_data[3]["service_name"]!)"
    
    }
    
    func load_data_by_tag(_ sender_tag:Int) {
        cloth_id = [String]()
        cloth_qty = [String]()
        cloth_name = [String]()
        cloth_cost = [String]()
        service_id = String()
        
        service_id = "\(dict_data[sender_tag]["service_id"]!)"

        var dict_arrr = "\(dict_data[sender_tag]["cloth"]!)".strign_to_dict()
        
        for i in 0..<dict_arrr.count{
            let dict = dict_arrr[i]
            cloth_id.append("\(dict["cloth_id"]!)")
            cloth_qty.append("\(dict["cloth_qty"]!)")
            cloth_name.append("\(dict["cloth_name"]!)")
            cloth_cost.append("\(dict["cloth_cost"]!)")
        }
        
        self.tbl_View.reloadData()
        
    }
}

extension items_VC: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cloth_id.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "items_TableCell", for: indexPath) as! items_TableCell
        cell.lbl_item_name.text = self.cloth_name[indexPath.row]
        cell.lbl_quantity.text = self.cloth_qty[indexPath.row]
        cell.lbl_price.text = self.cloth_cost[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 66
    }
}

extension items_VC{
    // MARK: Pickup_Order
    
    func decode_order_json() {
        let data = order_json.data(using: .utf8)!
        do {
            if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [Dictionary<String,Any>]
            {
                print(jsonArray)
                dict_data = jsonArray
            } else {
                print("bad json")
            }
        } catch let error as NSError {
            print(error)
        }
    }
}

extension String{
    func strign_to_dict() -> [Dictionary<String,Any>]{
        let data = self.data(using: .utf8)!
        do {
            if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [Dictionary<String,Any>]
            {
                print(jsonArray)
                return jsonArray
            } else {
                print("bad json")
                return [["error":true]]
            }
        } catch let error as NSError {
            print(error)
            return [["error":true]]
        }
    }
}
